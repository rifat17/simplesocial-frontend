import axios from "axios";
import { useReducer, useEffect } from "react";
export const AUTH_ACTIONS = {
  SIGNIN: "signi",
  SIGNUP: "signup",
  LOGOUT: "logout",
};

const state = {
  user: "",
};
const AuthReducer = (state, action) => {
  switch (action.type) {
    case AUTH_ACTIONS.SIGNIN:
      return { ...state, user: action.payload };
    case AUTH_ACTIONS.SIGNUP:
      return { ...state, user: action.payload };
    case AUTH_ACTIONS.LOGOUT:
      return { ...state, user: "" };
    default:
      break;
  }
};
export default function useAuth() {
  const [user, dispatch] = useReducer(AuthReducer, state);

  return [user, dispatch];
}
