import { useReducer } from "react";

const initialState = {
  loading: true,
};

function loaderReducer(state, action) {
  switch (action.type) {
    case "start":
      return { ...state, loading: true };
    case "stop":
      return { ...state, loading: false };
    default:
      return state;
  }
}

function useLoaderTime() {
  const [loader, setLoader] = useReducer(loaderReducer, initialState);

  const { loading } = loader;

  return [loading, setLoader];
}

export default useLoaderTime;
