import axios from "axios";
import { useReducer, useEffect } from "react";
export const POST_ACTIONS = {
  ADD_ALL: "add-all",
  ADD_POST: "add-post",
};

const state = [];
const PostReducer = (state, action) => {
  switch (action.type) {
    case POST_ACTIONS.ADD_ALL:
      return [...action.payload];
    case POST_ACTIONS.ADD_POST:
      return [action.payload, ...state];
    default:
      break;
  }
};
export default function usePosts() {
  const [posts, dispatch] = useReducer(PostReducer, state);
  useEffect(() => {
    async function fetchPosts() {
      await axios
        .get("http://localhost:3000/posts", {
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/json",
          },
        })
        .then((response) => {
          console.log(response);
          dispatch({
            type: POST_ACTIONS.ADD_ALL,
            payload: response.data.posts,
          });
        })
        .catch((msg) => console.log(msg));
    }
    fetchPosts();
  }, []);

  return [posts, dispatch];
}
