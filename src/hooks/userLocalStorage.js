import { useReducer, useEffect } from "react";
const ACTIONS = {
  GET_USERNAME: "get-username",
  ADD_USERNAME: "get-username",
};

const state = {};
const LocalStorageReducer = (state, action) => {
  switch (action.type) {
    case ACTIONS.GET_USERNAME:
      return state;
    case ACTIONS.ADD_USERNAME:
      console.log("actions", action);
      return { ...state, ...action.payload };
    default:
      return state;
  }
};
export default function useLocalStorage() {
  const [username, dispatch] = useReducer(LocalStorageReducer, state);
  useEffect(() => {
    let username = localStorage.getItem("social_user");
    console.log(username);
    dispatch({ type: ACTIONS.ADD_USERNAME, payload: { username: username } });
  }, []);

  return [username, dispatch];
}
