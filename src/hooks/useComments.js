import { useReducer, useEffect } from "react";
import { fetchCommentFromLambda } from "../components/comments/networkcall/commentAPI";
export const ACTIONS = {
  ADD_COMMENT: "add-comment",
  ADD_ALL: "add-all",
};

const state = [];

const CommentReducer = (state, action) => {
  switch (action.type) {
    case ACTIONS.ADD_COMMENT:
      return [action.payload, ...state];
    case ACTIONS.ADD_ALL:
      return [...action.payload];
    default:
      return state;
  }
};

function fetchComments(postID, dispatch) {
  fetchCommentFromLambda(postID)
    .then((response) => {
      // console.log(response.data);
      dispatch({
        type: ACTIONS.ADD_ALL,
        payload: response.data.comments,
      });
    })
    .catch((error) => {
      console.log(error);
    });
}

const useComments = ({ postID }) => {
  // console.log("CHANGED POSTID", postID);
  const [comments, dispatch] = useReducer(CommentReducer, state);
  useEffect(() => {
    fetchComments(postID, dispatch);
  }, [postID]);

  return [comments, dispatch];
};

export default useComments;
