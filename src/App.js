import "./App.css";
import LoginPlain from "./components/LoginPlain/LoginPlain";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import SignIn from "./components/views/Authentication/SignIn/SignIn";
import Home from "./components/views/Home/Home";
import ProtectedRoute from "./components/ProtectedRoute/ProtectedRoute";
import Auth from "./components/views/Authentication/Auth";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/signin" component={SignIn} />
        <ProtectedRoute exact path="/" component={Home} />
        <Route path="*" component={SignIn} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
