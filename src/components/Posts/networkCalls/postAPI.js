import axios from "axios";

export function POST_userPostData(postData) {
  console.log("post", postData);

  return new Promise((resolve, reject) => {
    const data = JSON.stringify(postData);

    const config = {
      method: "post",
      url: "http://localhost:3000/post",
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };

    axios(config)
      .then((response) => {
        console.log(response);
        resolve(response);
      })
      .catch((error) => {
        console.log(error.response);
        reject(error);
      });
  });
}
