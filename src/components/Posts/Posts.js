import Moment from "react-moment";
import Comments from "../comments/commentsv2";
import { ToastProvider } from "react-toast-notifications";

import styles from "./Posts.module.css";
import usePosts from "../../hooks/usePosts";
import PostForm from "./PostForm";
import useLoaderTime from "../../hooks/useLoaderTime";

function Posts() {
  const [posts, postsDispatch] = usePosts();
  const [loading, setLoader] = useLoaderTime();
  console.log(loading);

  const timeout = setTimeout(() => {
    setLoader({ type: "stop" });
  }, 5000);
  if (!loading) {
    clearTimeout(timeout);
  }

  let content = loading ? (
    <div className="d-flex justify-content-center">
      <div className="spinner-grow" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    </div>
  ) : (
    posts.map((post, idx) => (
      <div className="card w-75" key={idx}>
        <div className="card-body">
          <h5 className="card-title m-0">{post.postOwner.split("#")[1]}</h5>
          {!post.showMetadata ? (
            <>
              <span className="badge badge-primary m-2 badge-dark">
                {post.postType}
              </span>
              <small className="card-subtitle text-muted">
                <Moment fromNow>{post.posted_at}</Moment>
              </small>
            </>
          ) : (
            <small className="card-subtitle text-muted">(hide)</small>
          )}
          <p className="card-text">{post.postContent}</p>
          <ToastProvider>
            <Comments postID={post.SK} />
          </ToastProvider>
        </div>
      </div>
    ))
  );

  return (
    <div>
      <ToastProvider>
        <PostForm postsDispatch={postsDispatch} />
      </ToastProvider>
      <hr />
      <h3>Newsfeed</h3>
      {content}
    </div>
  );
}

export default Posts;
