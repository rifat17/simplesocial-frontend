import React, { useState, useReducer } from "react";
import { useToasts } from "react-toast-notifications";
import { POST_userPostData } from "./networkCalls/postAPI";
import { POST_ACTIONS } from "../../hooks/usePosts";

import moment from "moment";

const initialData = {
  post: "",
  postType: "post",
  postTypeOptions: [
    {
      label: "Post",
      value: "post",
    },
    {
      label: "Article",
      value: "article",
    },
  ],
  checked: false,
  isLoading: false,
  error: "",
};

function postReducer(state, action) {
  switch (action.type) {
    case "post":
      return {
        ...state,
        isLoading: true,
        error: "",
      };
    case "success":
      return {
        ...state,
        post: "",
        postType: "post",
        checked: false,
        isLoading: false,
        error: "",
      };

    case "error":
      return {
        ...state,
        error: action.payload.error,
        isLoading: false,
      };
    case "field":
      return {
        ...state,
        [action.field]: action.value,
      };

    default:
      return state;
  }
}

function PostForm({ postsDispatch }) {
  const [state, postDispatch] = useReducer(postReducer, initialData);

  const { post, postType, postTypeOptions, checked, isLoading } = state;

  const { addToast } = useToasts();

  const handleOnSubmit = (e) => {
    e.preventDefault();
    postDispatch({ type: "post" });

    const postData = {
      postOwner: localStorage.getItem("social_user"),
      postContent: post,
      postType: postType,
      showMetadata: checked,
      posted_at: moment().format("Y-MM-DDTHH:mm:ss"),
    };

    async function postUserPost() {
      try {
        const response = await POST_userPostData(postData);
        postDispatch({
          type: "success",
        });
        addToast("Posted!", {
          appearance: "info",
          autoDismiss: true,
        });
        // console.log(response.data);
        postsDispatch({
          type: POST_ACTIONS.ADD_POST,
          payload: response.data,
        });
      } catch (error) {
        postDispatch({
          type: "error",
          payload: {
            error: error.response,
          },
        });
        addToast(`Error! : ${error.response}`, {
          appearance: "error",
          autoDismiss: true,
        });
      }
    }

    postUserPost();
  };

  const isEnabled = post.length > 0;

  //   console.log(post, postType, postTypeOptions, checked, isLoading);
  return (
    <div>
      <form onSubmit={handleOnSubmit}>
        <div className="input-group m-3 w-75">
          <textarea
            className="form-control"
            placeholder="Whats on your mind?"
            aria-label="With textarea"
            required={true}
            value={post}
            onChange={(e) => {
              postDispatch({
                type: "field",
                field: "post",
                value: e.target.value,
              });
            }}
          ></textarea>
          <div className="input-group-append"></div>
        </div>
        <div className="form-row align-items-center ml-3">
          <div className="col-auto my-1">
            <select
              className="custom-select"
              value={postType}
              onChange={(e) => {
                postDispatch({
                  type: "field",
                  field: "postType",
                  value: e.target.value,
                });
              }}
            >
              {postTypeOptions.map((op) => (
                <option value={op.value}>{op.label}</option>
              ))}
            </select>
          </div>
          <div className="col-auto my-1">
            <div className="custom-control custom-checkbox">
              <input
                type="checkbox"
                className="custom-control-input"
                id="customControlAutosizing"
                defaultChecked={checked}
                onChange={(e) => {
                  postDispatch({
                    type: "field",
                    field: "checked",
                    value: e.target.value,
                  });
                }}
              />
              <label
                className="custom-control-label"
                htmlFor="customControlAutosizing"
              >
                Hide
              </label>
            </div>
          </div>
          <div className="col-auto my-1">
            <button
              disabled={isLoading}
              className="btn btn-outline-primary"
              type="submit"
            >
              {isLoading ? "Posting.." : "Post"}
            </button>
          </div>
        </div>
      </form>
    </div>
  );
}

export default PostForm;
