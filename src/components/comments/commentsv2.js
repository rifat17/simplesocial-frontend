import Moment from "react-moment";

import useComments from "../../hooks/useComments";
import AddCommentForm from "./AddCommentForm";

function Comments({ postID }) {
  const [comments, dispatch] = useComments({
    postID: postID,
  });

  let last5Comments = comments;
  const commentLength = comments.length;

  let moreComment = false;
  if (commentLength > 5) {
    last5Comments = comments.slice(0, 5);
    moreComment = true;
  }
  let content = last5Comments.length ? (
    <ul className="list-group list-group-flush">
      {last5Comments.map((comment, idx) => (
        <li className="list-group-item" key={comment.PK}>
          <h5 className="card-title m-0">
            {comment.commentedBy.split("#")[1]}
          </h5>
          {comment.comment}
          <br />
          <small className="card-subtitle text-muted font-italic">
            <Moment fromNow>{comment.commentedAt}</Moment>
          </small>
        </li>
      ))}
    </ul>
  ) : (
    ""
  );

  return (
    <div className="card">
      {moreComment ? (
        <span style={{ color: "#0645AD", textAlign: "right" }}>
          load more comment..
        </span>
      ) : (
        ""
      )}
      <span>comments ({commentLength})</span>
      {content}
      <hr style={{ margin: 0, padding: 0 }} />
      <small className="card-subtitle text-muted mt-1">Add Comments</small>
      <AddCommentForm dispatch={dispatch} postID={postID} />
    </div>
  );
}

export default Comments;
