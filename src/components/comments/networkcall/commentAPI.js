import axios from "axios";

export function POSTCommentToLambda(commentdata) {
  return new Promise((resolve, reject) => {
    // console.log("POSTCommentToLambda called");

    const data = JSON.stringify(commentdata);
    console.log("POSTCOMMENT", data);

    const config = {
      method: "post",
      url: "http://localhost:3000/post/comment",
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };

    axios
      .post(config.url, config.data)
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        console.log(error);
        reject(error);
      });
  });
}

export function fetchCommentFromLambda(postID) {
  return new Promise((resolve, reject) => {
    // console.log("fetchCommentFROMLambda called");

    axios
      .get("http://localhost:3000/post/comments", {
        params: {
          postID: postID,
        },
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        resolve(response);
      })
      .catch((msg) => reject(msg));
  });
}
