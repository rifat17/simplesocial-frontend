import axios from "axios";
import React, { useEffect, useReducer } from "react";

const ACTIONS = {
  ADD_ALL: "add-all",
};
const state = [];
const CommentReducer = (state, action) => {
  switch (action.type) {
    case ACTIONS.ADD_ALL:
      return [...action.payload];
    default:
      break;
  }
};

function Comments({ postID }) {
  const [comments, dispatch] = useReducer(CommentReducer, state);

  console.log("Comments", comments);

  useEffect(() => {
    async function fetchComments() {
      await axios
        .get("http://localhost:3000/post/comments", {
          params: {
            postID: postID,
          },
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/json",
          },
        })
        .then((response) => {
          console.log(response);
          dispatch({
            type: ACTIONS.ADD_ALL,
            payload: response.data.comments,
          });
        })
        .catch((msg) => console.log(msg));
    }
    fetchComments();
  }, [postID]);

  let content = comments
    ? comments.map((comment) => (
        <div className="card" key={comment.PK}>
          <div className="card-body">
            <span>{comment.commentedBy}</span>: {comment.comment}
          </div>
          <span style={{ color: "gray" }}>{comment.commentedAt}</span>
        </div>
      ))
    : "";

  return <div>{content}</div>;
}

export default Comments;
