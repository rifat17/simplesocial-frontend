import React, { useState } from "react";

import { ACTIONS } from "../../hooks/useComments";
import { POSTCommentToLambda } from "./networkcall/commentAPI";

import moment from "moment";
import { useToasts } from "react-toast-notifications";

function AddCommentForm({ dispatch, postID }) {
  const [reply, setReply] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const { addToast } = useToasts();

  const handleOnSubmit = (e) => {
    e.preventDefault();

    const commentData = {
      postID: postID,
      comment: reply,
      commentedBy: localStorage.getItem("social_user"),
      commentedAt: moment().format("Y-MM-DDTHH:mm:ss"),
    };
    setReply("");
    setIsLoading(true);
    async function postComment() {
      try {
        const response = await POSTCommentToLambda(commentData);

        setIsLoading(false);

        dispatch({
          type: ACTIONS.ADD_COMMENT,
          payload: response.data,
        });
        addToast("Comment added!", {
          appearance: "info",
          autoDismiss: true,
        });
      } catch (error) {
        addToast("ERROR adding comment", {
          //error.response.statusText
          appearance: "error",
          autoDismiss: true,
        });
      }
    }
    postComment();
  };
  const isEnabled = reply.length > 0;

  return (
    <>
      <form onSubmit={handleOnSubmit}>
        <div className="input-group">
          <div className="input-group-prepend">
            <span className="input-group-text">Reply</span>
          </div>
          <textarea
            className="form-control"
            aria-label="With textarea"
            required={true}
            value={reply}
            onChange={(e) => {
              setReply(e.target.value);
            }}
          ></textarea>
          <div className="input-group-append">
            <button
              disabled={!isEnabled}
              className="btn btn-outline-secondary"
              type="submit"
              aria-describedby="inputGroup-sizing-sm"
            >
              {isLoading ? "Commenting.." : "Comment"}
            </button>
          </div>
        </div>
      </form>
    </>
  );
}

export default AddCommentForm;
