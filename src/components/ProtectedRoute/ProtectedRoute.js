import React from "react";
import { Redirect, Route } from "react-router-dom";

function ProtectedRoute({ component: Compenent, ...restOfProps }) {
  const isAuthenticated = localStorage.getItem("isAuthenticated");
  console.log(isAuthenticated);

  return (
    <Route
      {...restOfProps}
      render={(props) =>
        isAuthenticated ? <Compenent {...props} /> : <Redirect to="/signin" />
      }
    />
  );
}

export default ProtectedRoute;
