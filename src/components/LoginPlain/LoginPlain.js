import React, { useState, useReducer } from "react";

import { login } from "../../utils/login/login";

import classes from "./LoginPlain.module.css";

const ACTIONS = {
  LOGIN: "login",
  LOGOUT: "logout",
  SUCCESS: "success",
  ERROR: "error",
  FIELD: "field",
};

function loginReducer(state, action) {
  switch (action.type) {
    case ACTIONS.FIELD:
      return {
        ...state,
        [action.payload.field]: action.payload.value,
      };
    case ACTIONS.LOGIN:
      return {
        ...state,
        error: "",
        isLoading: true,
      };
    case ACTIONS.LOGOUT:
      return {
        ...state,
        isLoggedIn: false,
        username: "",
        password: "",
      };
    case ACTIONS.SUCCESS:
      return {
        ...state,
        isLoggedIn: true,
        isLoading: false,
        error: "",
        username: "",
        password: "",
      };
    case ACTIONS.ERROR:
      return {
        ...state,
        error: "Incorrect username or password.",
        isLoading: false,
      };
    default:
      return state;
  }
}

const initialState = {
  username: "",
  password: "",
  isLoading: false,
  error: "",
  isLoggedIn: false,
};

function LoginPlain() {
  const [state, dispatch] = useReducer(loginReducer, initialState);
  const { username, password, isLoading, error, isLoggedIn } = state;
  const onSubmitHandler = async (e) => {
    e.preventDefault();

    dispatch({ type: ACTIONS.LOGIN });
    try {
      await login({ username, password });
      dispatch({ type: ACTIONS.SUCCESS });
    } catch (error) {
      dispatch({ type: ACTIONS.ERROR });
    }
  };

  return (
    <div className={classes.App}>
      {isLoggedIn ? (
        <>
          <h1>Hello {username}!</h1>
          <button onClick={(e) => dispatch({ type: ACTIONS.LOGOUT })}>
            Log Out
          </button>
        </>
      ) : (
        <div className={classes.login_container}>
          <form onSubmit={onSubmitHandler}>
            {error && <p className={classes.error}> {error} </p>}
            <p>Please Login!</p>
            <input
              type="text"
              placeholder="username"
              value={username}
              onChange={(e) =>
                dispatch({
                  type: ACTIONS.FIELD,
                  payload: { field: "username", value: e.target.value },
                })
              }
            />
            <input
              type="password"
              placeholder="password"
              autoComplete="new-password"
              value={password}
              onChange={(e) =>
                dispatch({
                  type: ACTIONS.FIELD,
                  payload: { field: "password", value: e.target.value },
                })
              }
            />
            <button
              className={classes.submit}
              type="submit"
              disabled={isLoading}
            >
              {isLoading ? "Logging in..." : "Log In"}
            </button>
          </form>
        </div>
      )}
    </div>
  );
}

export default LoginPlain;
