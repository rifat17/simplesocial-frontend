import React, { useReducer } from "react";
import { userSignup } from "../networkCalls/AuthAPI";
import { adduserToLocalStorage } from "../SignIn/SignIn";

async function signup(user, userDispatch) {
  if (user.password !== user.confirm_password) {
    userDispatch({
      type: "error",
      payload: "password mismatch",
    });
    return;
  }
  try {
    const response = await userSignup(user);
    console.log(response);
    userDispatch({
      type: "success",
    });

    adduserToLocalStorage(user.username);
  } catch (error) {
    console.log(error);
  }
}

const initState = {
  username: "",
  password: "",
  confirm_password: "",
  age: "",
  address: "",
  isLoading: false,
  error: "",
};

function signupReducer(state, action) {
  switch (action.type) {
    case "field":
      return {
        ...state,
        [action.field]: action.value,
      };
    case "signup":
      return {
        ...state,
        isLoading: true,
      };
    case "success":
      return {
        ...state,
        username: "",
        password: "",
        age: "",
        address: "",
        isLoading: false,
        error: "",
      };
    case "error":
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    default:
      return state;
  }
}

function SignUpForm({ setLoginActivity }) {
  const [state, dispatch] = useReducer(signupReducer, initState);
  const {
    username,
    password,
    confirm_password,
    age,
    address,
    isLoading,
    error,
  } = state;

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch({
      type: "signup",
    });

    const user = {
      username: username,
      password: password,
      confirm_password: confirm_password,
      age: age,
      address: address,
    };
    signup(user, dispatch);
  };
  return (
    <div className="text-center">
      <h1>User Signup</h1>
      <form
        onSubmit={handleSubmit}
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <div className="form-group">
          <label>Username</label>
          <input
            className="form-control"
            type="text"
            name="username"
            required={true}
            onChange={(e) =>
              dispatch({
                type: "field",
                field: "username",
                value: e.target.value,
              })
            }
          />
          <label>Age</label>
          <input
            className="form-control"
            type="number"
            name="age"
            required={true}
            onChange={(e) =>
              dispatch({
                type: "field",
                field: "age",
                value: e.target.value,
              })
            }
          />
          <label>Address</label>
          <input
            className="form-control"
            type="text"
            name="age"
            required={true}
            onChange={(e) =>
              dispatch({
                type: "field",
                field: "address",
                value: e.target.value,
              })
            }
          />
          <label>Password</label>
          <input
            className="form-control"
            type="password"
            name="password"
            required={true}
            onChange={(e) =>
              dispatch({
                type: "field",
                field: "password",
                value: e.target.value,
              })
            }
          />
          <label>Confirm Password</label>
          <input
            className="form-control"
            type="password"
            name="confirm_password"
            required={true}
            onChange={(e) =>
              dispatch({
                type: "field",
                field: "confirm_password",
                value: e.target.value,
              })
            }
          />
          <span style={{ color: "red" }} className="warn">
            {error}
          </span>
          <button
            type="submit"
            disabled={isLoading}
            className="btn btn-primary"
          >
            {isLoading ? "Signing Up.." : "SignUp"}
          </button>
          <button
            type="submit"
            className="btn btn-primary m-2"
            onClick={(e) => {
              e.preventDefault();
              setLoginActivity(true);
            }}
          >
            SignIn
          </button>
        </div>
      </form>
    </div>
  );
}

export default SignUpForm;
