import React, { useState } from "react";
import SignUpForm from "./SignUpForm";

function SignUp() {
  const [userDate, setUserDate] = useState({ username: "", password: "" });
  const [errorMessage, setErrorMessage] = useState({ value: "" });

  const handleInoutChange = (e) => {
    setUserDate((prevState) => {
      return {
        ...prevState,
        [e.target.name]: e.target.value,
      };
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (userDate.username === "" || userDate.password === "") {
      setErrorMessage((prevState) => ({
        value: "Empty username/password field",
      }));
    } else if (userDate.username === "admin" && userDate.password === "12345") {
      localStorage.setItem("isAuthenticated", "true");
      localStorage.setItem("social_user", `USER#${userDate.username}`);
      window.location.pathname = "/";
    } else {
      setErrorMessage((prevState) => ({ value: "Invalid username/password" }));
      return;
    }
  };

  return (
    <SignUpForm
      handleInoutChange={handleInoutChange}
      handleSubmit={handleSubmit}
    />
  );
}

export default SignUp;
