import React, { useState } from "react";
import SignUpForm from "../SignUp/SignUpForm";
import SignInForm from "./SignInForm";

export function adduserToLocalStorage(username) {
  localStorage.setItem("isAuthenticated", "true");
  localStorage.setItem("social_user", `USER#${username}`);
  window.location.pathname = "/";
}

function SignIn() {
  const [loginActivity, setLoginActivity] = useState(true);

  let content = loginActivity ? (
    <SignInForm setLoginActivity={setLoginActivity} />
  ) : (
    <SignUpForm setLoginActivity={setLoginActivity} />
  );

  return <div>{content}</div>;
}

export default SignIn;
