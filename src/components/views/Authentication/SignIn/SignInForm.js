import React, { useReducer } from "react";
import { userSignin } from "../networkCalls/AuthAPI";
import { adduserToLocalStorage } from "../SignIn/SignIn";

async function signin(user, userDispatch) {
  try {
    const response = await userSignin(user);
    console.log(response);
    userDispatch({
      type: "success",
    });

    adduserToLocalStorage(user.username);
  } catch (error) {
    console.log(error);
    userDispatch({
      type: "error",
      payload: "Error!",
    });
  }
}

const initState = {
  username: "",
  password: "",
  isLoading: false,
  error: "",
};

function signinReducer(state, action) {
  switch (action.type) {
    case "field":
      return {
        ...state,
        [action.field]: action.value,
      };
    case "signin":
      return {
        ...state,
        isLoading: true,
        error: "",
      };
    case "success":
      return {
        ...state,
        username: "",
        password: "",
        isLoading: false,
        error: "",
      };
    case "error":
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    default:
      return state;
  }
}

function SignInForm({ setLoginActivity }) {
  const [state, dispatch] = useReducer(signinReducer, initState);
  const { username, password, isLoading, error } = state;

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch({
      type: "signin",
    });
    const user = {
      username: username,
      password: password,
    };
    signin(user, dispatch);
  };
  return (
    <div className="text-center">
      <h1>User Signin</h1>
      <form
        onSubmit={handleSubmit}
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <div className="form-group">
          <label>Username</label>
          <input
            className="form-control"
            type="text"
            name="username"
            required={true}
            onChange={(e) =>
              dispatch({
                type: "field",
                field: "username",
                value: e.target.value,
              })
            }
          />

          <label>Password</label>
          <input
            className="form-control"
            type="password"
            name="password"
            required={true}
            onChange={(e) =>
              dispatch({
                type: "field",
                field: "password",
                value: e.target.value,
              })
            }
          />
          <span style={{ color: "red" }} className="warn">
            {error}
          </span>
          <button
            type="submit"
            disabled={isLoading}
            className="btn btn-primary"
          >
            {isLoading ? "Signing in.." : "SignIn"}
          </button>
          <button
            type="submit"
            className="btn btn-primary m-2"
            onClick={(e) => {
              e.preventDefault();
              setLoginActivity(false);
            }}
          >
            SignUp
          </button>
        </div>
      </form>
    </div>
  );
}

export default SignInForm;
