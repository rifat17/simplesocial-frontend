import React from "react";
import { Router, Switch, Route, Link } from "react-router-dom";
import SignIn from "./SignIn/SignIn";
import SignUp from "./SignUp/SignUp";

function Auth() {
  return (
    <div>
      <SignIn />
      <SignUp />
    </div>
  );
}

export default Auth;

// <div>
//       <nav className="navbar navbar-expand navbar-dark bg-dark">
//         SimpleSocial
//         <div className="navbar-nav mr-auto">
//           <li className="nav-item">Home</li>
//         </div>
//         {false ? (
//           <div className="navbar-nav ml-auto">
//             <li className="nav-item">"Hasib"</li>
//             <li className="nav-item">LogOut</li>
//           </div>
//         ) : (
//           <div className="navbar-nav ml-auto">
//             <li className="nav-item">Login</li>

//             <li className="nav-item">Sign Up</li>
//           </div>
//         )}
//       </nav>
//       <div className="container mt-3"></div>
//       <SignIn />
//     </div>
