import axios from "axios";

export function userSignin(userData) {
  return new Promise((resolve, reject) => {
    const data = JSON.stringify(userData);
    // console.log("Login", data);

    const config = {
      method: "post",
      url: "http://localhost:3000/user/login",
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };

    axios(config)
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        console.log(error);
        reject(error);
      });
  });
}

export function userSignup(userData) {
  return new Promise((resolve, reject) => {
    const data = JSON.stringify(userData);
    // console.log("Login", data);

    const config = {
      method: "post",
      url: "http://localhost:3000/user",
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };
    axios(config)
      .then((response) => {
        resolve(response);
      })
      .catch((msg) => reject(msg));
  });
}
