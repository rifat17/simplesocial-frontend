import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { getAllPost } from "../../../store/actions/TestAction";
import Users from "../../Users/Users";
import Posts from "../../Posts/Posts";
import { getAllSocialMediaPost } from "../../../store/actions/PostAction";

function Home() {
  const handleLogout = (e) => {
    e.preventDefault();
    localStorage.setItem("isAuthenticated", "");
    localStorage.setItem("social_user", "");
    window.location.pathname = "/signin";
  };
  const getUsername = () => {
    return localStorage.getItem("social_user");
  };

  return (
    <div className="App">
      <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand">SimpleSocial</a>
        <div class="form-inline">
          ({getUsername().split("#")[1]})
          <button
            class="btn btn-outline-success my-2 my-sm-0"
            onClick={handleLogout}
            type="button"
          >
            Logout
          </button>
        </div>
      </nav>
      <Posts />
    </div>
  );
}

export default Home;
