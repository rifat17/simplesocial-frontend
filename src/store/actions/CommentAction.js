import axios from "axios";
import { COMMENT_ACTIONS } from "../reducers/CommentReducer";

export const getAllCommentsByPostID = (postID) => {
  console.log("getAllCommentsByPostID", postID);
  return (dispatch) => {
    axios
      .get("http://localhost:3000/post/comments", {
        params: {
          postID: postID,
        },
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        console.log(response);
        dispatch({
          type: COMMENT_ACTIONS.GET_ALL,
          payload: response.data.comments,
        });
      })
      .catch((msg) => console.log(msg));
  };
};
