import axios from "axios";
import { POST_ACTIONS } from "../reducers/PostReducer";

export const getAllSocialMediaPost = () => {
  return (dispatch) => {
    axios.get("http://localhost:3000/posts").then((response) => {
      dispatch({
        type: POST_ACTIONS.GET_ALL_POSTS,
        payload: response.data.posts,
      });
    });
  };
};
