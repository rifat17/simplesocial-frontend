import { combineReducers } from "redux";
import TestReducer from "./TestReducer";
import PostReducer from "./PostReducer";
import CommentReducer from "./CommentReducer";

const reducers = combineReducers({
  Test: TestReducer,
  Posts: PostReducer,
  Comments: CommentReducer,
});

export default reducers;
