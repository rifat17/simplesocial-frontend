const initialState = {
  posts: null,
};

export const POST_ACTIONS = {
  ADD: "add-post",
  GET_ALL_POSTS: "get-all-posts",
};

export default function PostReducer(state = initialState, action) {
  switch (action.type) {
    case POST_ACTIONS.ADD:
      return {
        ...state,
        posts: [...state, action.payload],
      };
    case POST_ACTIONS.GET_ALL_POSTS:
      return {
        ...state,
        posts: action.payload,
      };

    default:
      return state;
  }
}
