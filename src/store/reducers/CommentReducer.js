const initialState = {
  comments: null,
};

export const COMMENT_ACTIONS = {
  ADD: "add-comment",
  GET_ALL: "get-all-comment",
};

export default function CommentReducer(state = initialState, action) {
  switch (action.type) {
    case COMMENT_ACTIONS.ADD:
      return {
        ...state,
        posts: [...state, action.payload],
      };
    case COMMENT_ACTIONS.GET_ALL:
      return {
        ...state,
        comments: action.payload,
      };

    default:
      return state;
  }
}
